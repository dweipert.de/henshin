- https://www.oreilly.com/library/view/flex-bison/9780596805418/ch01.html
- https://raw.githubusercontent.com/richarddzh/gnu-flex-manual/master/flex.pdf
- https://ftp.gnu.org/old-gnu/Manuals/flex-2.5.4/html_node/flex_toc.html

- https://www.ecosia.org/search?q=transpiler%20with%20bison%20and%20lex
- https://github.com/labis7/Flex_Bison_Transpiler
- https://gnuu.org/2009/09/18/writing-your-own-toy-compiler/
- https://lloydrochester.com/post/flex-bison/json-parse-ast/

- https://news.ycombinator.com/item?id=37252334
- https://tomassetti.me/why-you-should-not-use-flex-yacc-and-bison/
- http://www.dabeaz.com/ply/ply.html#ply_nn27
